(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

module Make(Config : sig
    val allow_big_map : bool
  end) : sig
  val structure : IML.t -> Michelson.Opcode.t list

  val constant : IML.t -> Michelson.Constant.t option
end

val clean_field_annot : Michelson.Type.t -> Michelson.Type.t
